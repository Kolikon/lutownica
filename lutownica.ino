#include "main.h"


int main(void)
{
  Serial.begin(9600);
  ports_init();
  timers_init();
  adc_init();
  display_init();
  
  uint16_t temperature_real, temperature_set;
  int16_t PID_I, PID_error;
  bool heating= 0;


  // wyswietl(wciśnij dowolny przycisk)
  display.clearDisplay();
  display.display();
  display.setCursor(0,0);
  display.println("puszuj batona");
  display.display();

  
  while( (PIND & 0b10000000) && (PINB & 0b10) )
  {}

  while(1)
  {

    temperature_real= thermocouple.readCelsius();
    
    // wyswietl(Temperature_real, temperature_set, czy jest heating itd.)
    display.clearDisplay();
    display.display();
    display.setCursor(0,0);
    display.print("Tc: ");
    display.println(temperature_real);
    display.print("Ts: ");
    display.println(temperature_set);
    display.print("HEATING: ");
    display.print(heating);
    display.display();
    

    if(heating)
    {
      OCR0A= PID(temperature_set,   //setpoint
               temperature_real,  //variable
               10,                //kP
               1,                 //kI
               0,                 //kD
               TCNT1,             //deltaT
               &PID_I,            //address of lastI
               &PID_error);       //address of lastError

      TCNT1= 0;

      delay(10);
    }
    else
    {
      OCR0A= 0;
    }

     
    if( !(PIND & 0b10000000) )                  //set-button pressed
    {
      temperature_set= 0.390625 * analog_read(0) + 50;
      delay(DEBOUNCE);
    }

    if( !heating && !(PINB & 0b10) )            //start-button pressed while not heating
    {
      heating= 1;
      delay(DEBOUNCE);
    }

    if( heating && !(PINB & 0b10) )            //start-button pressed while heating
    {
      TCNT1= 0;
      delay(DEBOUNCE);

      while(TCNT1 < 31250)    //wait 2s
      {}

      if( (PINB & 0b10) )
      {
        temperature_set= temperature_set / 2;
      }
      else
      {
        temperature_set= 0;
        heating= 0;
      }
      
    }
  }
}
