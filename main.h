#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <max6675.h>
#include <EEPROM.h>

#define width 128
#define height 64
#define reset 4
#define addr 0x3C

#define tcDO 12
#define tcCS 10
#define tcCLK 13

//Outputs
#define SSR     6          //PD6 OC0A
#define wrkLED  5          //PD5 OC0B

//Inputs
#define setBtnPin   7     //PD7
#define startBtnPin 9     //PB1
#define TempSetPin  A0    //PC0

#define longPress  2000
#define shortPress 1000
#define DEBOUNCE 200


volatile MAX6675 thermocouple(tcCLK, tcCS, tcDO);
volatile Adafruit_SSD1306 display(width, height, &Wire, reset);

void display_init()
{
  display.begin(SSD1306_SWITCHCAPVCC, addr);
  display.clearDisplay();
  display.display();
  display.setCursor(0,0);
  display.setTextSize(2);
  display.setTextColor(SSD1306_WHITE);
  display.clearDisplay();
  display.display();
}


void ports_init()
{
  
  DDRB= 0;          //PB1 input
  PORTB= 2;         //PB1 pull-up

  DDRC= 0;          //PC0 input
  PORTC= 0;         //no pull-ups
  
  DDRD=  0b01100000;  //pins 5 and 6 outputs, pin 7 input
  PORTD= 0b10000000;  //pin 7 pull-up  

}

void timers_init()
{

  //Timer 0 - 8 bit, used for PWM
  TCCR0A= (1 << COM0A1) | (1 << COM0B1) | (1 << WGM00);
  TCCR0B= 0b101;    //prescaler 1024 - PWM at ~61Hz

  //Timer 1 - 16 bit, used for buttons
  TCCR1A= 0;
  TCCR1B= 0b101;    //prescaler 1024 - 1 tick = 64 us
  
}

void adc_init()
{
  ADMUX= 0;   //Reference - AREF
  ADCSRA= (1 << ADEN);   //enable ADC
}

uint16_t analog_read(uint8_t channel)
{
  ADMUX= channel;
  ADCSRA|= (1 << ADSC);

  while( (ADCSRA & (1 << ADSC)) )
  {}

  return ADC;
}

uint8_t PID(uint16_t setpoint,
             uint16_t variable,
             int16_t kP,
             int16_t kI,
             int16_t kD, 
             uint16_t deltaT, 
             int16_t *lastI,
             int16_t *lastError)
{
  int16_t  P, I, D, error;
  uint8_t output;

  error= setpoint - variable;
  P= kP * error;
  I= kI * error * deltaT + *lastI;
  D= kD * (error - *lastError) / deltaT;

  output= P + I + D;
  
  *lastI= I;
  *lastError= error;
  
  return output;
}
